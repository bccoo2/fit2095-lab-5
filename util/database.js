const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

const mongoConnect = callback => {
    // dbuser
    // l6eASYPIUEk1dc8J44Gu56ZTH7q
    MongoClient.connect('mongodb+srv://dbuser:l6eASYPIUEk1dc8J44Gu56ZTH7q@cluster0-7h1s4.azure.mongodb.net/test?retryWrites=true&w=majority', {useNewUrlParser: true})
        .then(client => {
            console.log('Database Connected');
            _db = client.db();
            callback();
        })
        .catch(err => {
            console.log(err);
            throw err;
        });
};

const getDb = () => {
    if (_db)
        return _db;
    else
        throw 'No database connection';
};

const getCollection = () => {
    return 'lab5-collection';
}

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;
exports.getCollection = getCollection;