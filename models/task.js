const mongodb = require('mongodb');
const getDb = require('../util/database').getDb;
const getCollection = require('../util/database').getCollection;

/**
 * Class object for tasks. Easiest alternative than working with an ORM
 */
class Task {
    /**
     * Constructor
     * @param taskName
     * @param taskAssignee
     * @param taskDue
     * @param taskStatus
     * @param taskDescription
     * @param taskID
     */
    constructor(taskName, taskAssignee, taskDue, taskStatus, taskDescription, taskID) {
        this.taskName = taskName;
        this.taskAssignee = taskAssignee;
        this.taskDue = taskDue;
        this.taskStatus = taskStatus;
        this.taskDescription = taskDescription;
        this._id = taskID ? mongodb.ObjectId(taskID) : null;
    }

    /**
     * Save task object as a document
     * @returns {Promise<T>}
     */
    save() {
        const db = getDb();
        let dbOp;
        if (this._id) {
            // Update the product
            dbOp = db
                .collection(getCollection())
                .updateOne({ _id: this._id }, { $set: this });
        } else {
            dbOp = db.collection(getCollection()).insertOne(this);
        }
        return dbOp
            .then(result => {
                console.log(result);
            })
            .catch(err => {
                console.log(err);
            });
    }

    static update(id, status) {
        const db = getDb();
        return db
            .collection(getCollection())
            .updateOne({ _id: mongodb.ObjectID(id) }, { $set: {taskStatus: status} })
            .then(result => {
                console.log(result)
            })
            .catch(err => {
                throw err;
            });
    }

    /**
     * Get all tasks
     * @returns {Promise<T>}
     */
    static fetchAll() {
        const db = getDb();
        return db.collection(getCollection())
            .find()
            .toArray()
            .then(tasks => {
                console.log(tasks);
                return tasks;
            })
            .catch(err => {
                throw err;
            });
    }

    /**
     * Delete task by id
     * @param prodId id to delete
     * @returns {Promise<T>}
     */
    static deleteById(prodId) {
        const db = getDb();
        return db
            .collection(getCollection())
            .deleteOne({ _id: mongodb.ObjectId(prodId) })
            .then(result => {
                console.log('Deleted task ' + prodId);
            })
            .catch(err => {
                throw err;
            });
    }

    /**
     * Delete completed tasks
     * @returns {Promise<unknown>}
     */
    static deleteCompleted() {
        const db = getDb();
        return db
            .collection(getCollection())
            .deleteMany({taskStatus: 'Complete'})
            .then(result => {
                console.log('Deleted Completed Tasks');
            })
            .catch(err => {
                throw err;
            })
    }

    /**
     * Delete all tasks in collection
     * @returns {Promise<unknown>}
     */
    static deleteAll() {
        const db = getDb();
        return db
            .collection(getCollection())
            .deleteMany()
            .then(result => {
                console.log('Deleted All Tasks');
            })
            .catch(err => {
                throw err;
            })
    }
}

module.exports = Task;