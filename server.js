const express = require('express');
const router = require('./routes');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

const mongoConnect = require('./util/database').mongoConnect;

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/css', express.static(path.join(__dirname, 'public/stylesheets/')));

app.use('/', router);

mongoConnect(() => {
    app.listen(process.env.PORT || 4000, function(){
        console.log('Your node js server is running');
    });
    console.log("Server running at http://localhost:4000");
});
