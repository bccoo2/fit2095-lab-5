const express = require('express');
const router = express.Router();

const Task = require('../models/task');

router.get('/', function (req, res) {
    res.render('index', {pageTitle: 'Index'})
});

router.get('/newTask', function (req, res) {
    res.render('newTask', {pageTitle: 'New Task'})
});

router.post('/addTask', function (req, res) {
    let taskName = req.body.taskName;
    let taskAssignee = req.body.taskAssignee;
    let taskDue = req.body.taskDue;
    let taskStatus = req.body.taskStatus;
    let taskDescription = req.body.taskDescription;
    let task = new Task(taskName, taskAssignee, taskDue, taskStatus, taskDescription);
    task.save()
        .then(result => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.get('/listTasks', function (req, res) {
    Task.fetchAll()
        .then(tasks => {
            res.render("listTasks", {
                pageTitle: 'Task List',
                tasks: tasks
            });
        })
        .catch(err => {
            throw err;
        })
});

router.get('/deleteTask', function (req, res) {
    res.render('deleteTask', {pageTitle: 'Delete Task'})
});

router.post('/deleteTask', function (req, res) {
    const taskID = req.body.taskID;
    Task.deleteById(taskID)
        .then(() => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.post('/deleteCompletedTasks', function (req, res) {
    const taskID = req.body.taskID;
    Task.deleteCompleted()
        .then(() => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.post('/deleteAllTasks', function (req, res) {
    const taskID = req.body.taskID;
    Task.deleteAll()
        .then(() => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.get('/updateTask', function (req, res) {
    res.render('updateTask', {pageTitle: 'Update Task'})
});

router.post('/updateTask', function (req, res) {
    let taskID = req.body.taskID;
    let taskStatus = req.body.taskStatus;
    Task.update(taskID, taskStatus)
        .then(result => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

module.exports = router;